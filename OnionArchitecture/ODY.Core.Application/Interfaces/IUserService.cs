﻿using ODY.Core.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODY.Core.Application.Interfaces
{
    public interface IUserService
    {
        public UserEntity FindById(int id);

        public IEnumerable<UserEntity> FindAll();
    }
}
