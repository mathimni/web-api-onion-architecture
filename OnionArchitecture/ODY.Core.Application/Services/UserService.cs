﻿using ODY.Core.Application.Interfaces;
using ODY.Core.Domain.Entities;
using ODY.Core.Services.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODY.Core.Application.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public IEnumerable<UserEntity> FindAll()
        {
            return _userRepository.FindAll();
        }

        public UserEntity FindById(int id)
        {
            return _userRepository.FindById(id);
        }
    }
}
