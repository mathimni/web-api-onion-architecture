﻿using ODY.Core.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODY.Core.Services.Repositories
{
    public interface IUserRepository
    {
        public UserEntity FindById(int id);

        public IEnumerable<UserEntity> FindAll();
    }
}
