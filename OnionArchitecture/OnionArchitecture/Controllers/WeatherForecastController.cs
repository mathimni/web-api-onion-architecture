using Microsoft.AspNetCore.Mvc;
using ODY.Core.Application.Interfaces;
using ODY.Core.Domain.Entities;

namespace OnionArchitecture.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
        "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
    };

        private readonly ILogger<WeatherForecastController> _logger;
        
        private readonly IUserService _userService;

        public WeatherForecastController(ILogger<WeatherForecastController> logger, IUserService userService)
        {
            _logger = logger;
            _userService = userService; 
        }

        [HttpGet(Name = "GetUser")]
        public ActionResult<UserEntity> GetUser()
        {
            int userId = 1;
            UserEntity user = _userService.FindById(userId);

            if (user == null)
            {
                _logger.LogInformation("The user with id {0} was not found.", userId);
                return NotFound();
            }
            else
            {
                _logger.LogInformation("The user {0} was found.", user.Name);
                return Ok(user);
            }
        }
    }
}