﻿using Microsoft.Extensions.DependencyInjection;
using ODY.Core.Services.Repositories;
using ODY.Infrastructure.Repositories;

namespace ODY.Core.Infrastructure.DI
{
    public static class ServiceCollectionExtension
    {
        public static void AddInfrastructureServices(this IServiceCollection services)
        {
            services.AddScoped<IUserRepository, UserRepository>();
        }
    }
}
