﻿using ODY.Core.Domain.Entities;
using ODY.Core.Services.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODY.Infrastructure.Repositories
{
    internal class UserRepository : IUserRepository
    {
        private List<UserEntity> _inMemoryUsers;

        public UserRepository()
        {
            _inMemoryUsers = new List<UserEntity>()
            {
                new UserEntity { Id = 1, Name = "John Snow", Email = "snow@mail.com", IsActive = true},
                new UserEntity { Id = 2, Name = "James Bond", Email = "bond@mail.com", IsActive = true },
            };
        }

        public UserEntity FindById(int id)
        {
            return MapUser(_inMemoryUsers.SingleOrDefault(user => user.Id == id));
        }

        public IEnumerable<UserEntity> FindAll()
        {
            return _inMemoryUsers.ToArray();
        }

        private UserEntity MapUser(UserEntity userEntity)
        {
            if (userEntity == null)
                return null;

            return new UserEntity
            {
                Id = userEntity.Id,
                Name = userEntity.Name,
                Email = userEntity.Email,
                IsActive = userEntity.IsActive
            };
        }

    }
}
